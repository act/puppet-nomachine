#!/bin/bash
echo ---
echo nxgroups:
sudo /usr/NX/bin/nxserver --grouplist | tail -n +3 | awk ' \
  function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s } \
  { \
  if (NR == 1) { \
	  s[1]=1; \
	  for ( i = 1 ; i < NF ; i++) { \
		  l[i]=length($i); \
		  s[i+1]=s[i]+l[i]+1 \
	  } \
  } else { \
  	printf "  %s:\n", substr($0,s[1],l[1]); \
	printf "    priority: %s\n", rtrim(substr($0,s[2],l[2])); \
	redirect=rtrim(substr($0,s[3],l[3])); \
	if ( length(redirect) == 0 ) { \
		printf "    redirect: none\n"; \
	} else { \
	 	printf "    redirect: %s\n"; \
	} \
	n = split(substr($0,s[6],l[6]),members,","); \
        if ( n == 0 ) { \
		printf "    members: []\n" \
	}
	else {
  		printf "    members:\n"; \
		for ( i in members) { \
			printf "      - %s\n",members[i]; \
		} \
	} \
  } \
}'
