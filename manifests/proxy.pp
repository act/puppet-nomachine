#
class nomachine::proxy (
  Enum['absent','present'] $ensure = 'present',
) {

  if ( $ensure == 'present' ) {
     $ensure_service = 'running'
     $enable_service = true
  } else {
     $ensure_service = 'stopped'
     $enable_service = false
  }
  class { 'squid':
    ensure_service => $service_ensure,
    enable_service => $service_enable,
  }
  squid::auth_param { 'basic auth_param':
    scheme  => 'basic',
    entries => [
      'program /usr/lib/squid/basic_pam_auth',
      'children 5',
      'realm Squid Basic Authentication',
      'credentialsttl 5 hours',
    ],
  }
  squid::acl { 'password':
    type    => proxy_auth,
    entries => ['REQUIRE'],
  }
  squid::acl { 'Safe_ports':
    type    => port,
    entries => ['4000'],
  }
  squid::http_access { 'Safe_ports':
    action => allow,
  }
  squid::http_access{ '!Safe_ports':
    action => deny,
  }
  squid::acl { 'CONNECT':
    type    => 'method',
    entries => ['CONNECT'],
  }
  squid::acl { 'SSL_ports':
    type    => port,
    entries => ['4000'],
  }
  squid::http_access{ '!SSL_ports':
    action  => deny,
    value   => 'CONNECT !SSL_ports',
  }
  squid::http_access{ 'all':
    action  => allow,
    order   => '99',
  }

  squid::extra_config_section {'port setting':
    order => '60',
    config_entries => {
       'http_port' => 443,
    }
  }

}

