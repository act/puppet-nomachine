#
class nomachine::node_connection_timeout (
  Enum['absent','present'] $ensure = 'present',
  Integer $timeout = 120,
  ) {

  nomachine::config_line { 'NodeConnectionTimeout':
    ensure => $ensure,
    type   => 'node',
    key    => 'NodeConnectionTimeout',
    value  => "${timeout}",
  }

}

