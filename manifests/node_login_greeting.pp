#
class nomachine::node_login_greeting (
  Enum['absent','present'] $ensure = 'present',
  String $greeting = 'Welcome to your NoMachine session',
  ) {

  nomachine::config_line { 'NodeLoginGreeting':
    ensure => $ensure,
    type   => 'node',
    key    => 'NodeLoginGreeting',
    value  => "\"${greeting}\"",
  }

}

