#
class nomachine::physical_desktop_sharing (
  Enum['absent','present'] $ensure = 'present',
  String $share,
) {

  $value = $share ? {
    /(enable|true)/ => 1,
    true            => 1,
    default         => 0,
  }

  nomachine::config_line { 'PhysicalDesktopSharing':
    ensure => $ensure,
    type   => 'server',
    key    => 'PhysicalDesktopSharing',
    value  => "${value}",
    after  => '#PhysicalDesktopSharing 1',
  }

}

