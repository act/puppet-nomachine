#
class nomachine::rdp_session (
  Enum['absent','present'] $ensure = 'present',
  String $rdp_command = '/usr/bin/rdesktop -f',
) {

include stdlib

  nomachine::config_line { 'CommandStartRDP':
    ensure => $ensure,
    type   => 'node',
    key    => 'CommandStartRDP',
    value  => $rdp_command,
  }

}

