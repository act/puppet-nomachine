#
class nomachine::connect_policy (
  Enum['absent','present'] $ensure = 'present',
  Integer[0,1] $create  = 1,
  Integer[0,1] $connect = 1,
  Integer[0,1] $migrate = 1,
  Integer[0,1] $desktop = 0,
  Integer[0,1] $dialog  = 0,
) {

  nomachine::config_line { 'ConnectPolicy':
    ensure => $ensure,
    type   => 'server',
    key    => 'ConnectPolicy',
    value  => "autocreate=${create},autoconnect=${connect},automigrate=${migrate},desktop=${desktop},dialog=${dialog}",
  #  after  => '#ConnectionsUserLimit 20',
  }

}

