#
class nomachine::fail2ban::nxhtd (
  String  $ensure   = 'present',
  Boolean $enabled  = true,
  Integer $maxretry = 1,
  String  $action   = 'iptables-allports',
  Integer $findtime = 600,
  Integer $bantime  = 600,
) {
 
  include fail2ban

  $_enabled = $ensure ? {
    'present' => $enabled,
    default   => false
  } 
  file_line { 'NXDOS_Enabled':
    path  => '/usr/NX/etc/htd.cfg',
    line  => '    DOSEnabled          true',
    match => '^\s+DOSEnabled\s+',
    require => Package[$nomachine::pkgname],
  }
  exec { 'NXDOS_Enabled_restart':
    command => '/usr/NX/bin/nxserver --restart nxhtd',
    subscribe => File_line['NXDOS_Enabled'],
    refreshonly => true,
  }

  $failregx = @(EOREGX)
    ^.*\[client <HOST>(:\d{1,5})?\] client denied by server configuration:.*
    |-EOREGX
  fail2ban::jail { 'nxhtd-evasive':
    enabled            => $_enabled,
    filter_failregex   => $failregx,
    filter_ignoreregex => '',
    filter_includes    => '', #findtime bug
    port               => '4080,4443',
    logpath            => '/usr/NX/var/log/nxhtd-error.log',
    maxretry           => $maxretry,
    action             => $action,
    findtime           => $findtime,
    bantime            => $bantime,
  }

}
