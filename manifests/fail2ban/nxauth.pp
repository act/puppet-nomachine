#
class nomachine::fail2ban::nxauth (
  String  $ensure   = 'present',
  Boolean $enabled  = true,
  Integer $maxretry = 10,
  String  $action   = 'iptables-allports',
  Integer $findtime = 600,
  Integer $bantime  = 600,
) {
 
  include fail2ban

#  file { '/etc/fail2ban/filter.d/nxauth.conf':
#    ensure => $ensure,
#    source => 'puppet://modules/nomachine/fail2ban/nxauth.conf',
#    owner  => 'root',
#    group  => 'root',
#    mode   => '0644',
#    require => Class['fail2ban'],
#  }
 
  $_enabled = $ensure ? {
    'present' => $enabled,
    default   => false
  } 
  $additional_config = @(EOCONFIG)
    method=[^']*
    errorMsg=[^']*
    |EOCONFIG
  $failregx = @(EOREGX)
    ^.*ERROR! Authentication with '%(method)s' from host '<HOST>' failed\. Error is '%(errorMsg)s'\.$
    |-EOREGX
  fail2ban::jail { 'nxauth':
    enabled                  => $_enabled,
    filter_additional_config => $additional_config,
    filter_failregex         => $failregx,
    filter_ignoreregex       => '',
    filter_includes          => '', #findtime bug
    port                     => '4000',
    logpath                  => '/usr/NX/var/log/nxserver.log',
    maxretry                 => $maxretry,
    action                   => $action,
    findtime                 => $findtime,
    bantime                  => $bantime,
#    require         => File['/etc/fail2ban/filter.d/nxauth.conf'],
  }

}
