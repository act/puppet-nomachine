#
class nomachine::fail2ban::nxd (
  String  $ensure   = 'present',
  Boolean $enabled  = true,
  Integer $maxretry = 20,
  String  $action   = 'iptables-allports',
  Integer $findtime = 5,
  Integer $bantime  = 600,
) {
 
  include fail2ban

  $_enabled = $ensure ? {
    'present' => $enabled,
    default   => false
  } 
  $failregx = @(EOREGX)
    ^Info: Connection from <HOST> port \d+ accepted on.*$
    |-EOREGX
  fail2ban::jail { 'nxd':
    enabled            => $_enabled,
    filter_failregex   => $failregx,
    filter_ignoreregex => '',
    filter_includes    => '', #findtime bug
    port               => '4000',
    logpath            => '/usr/NX/var/log/nxserver.log',
    maxretry           => $maxretry,
    action             => $action,
    findtime           => $findtime,
    bantime            => $bantime,
  }

}
