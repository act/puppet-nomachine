#
class nomachine::debug (
  Enum['absent','present'] $ensure,
  Enum[
    'terminal-server-node',
    'enterprise-server',
    'enterprise-terminal-server',
    'enterprise-terminal-server-cluster'
    ] $node_type,
    Boolean $debug = true,
    Boolean $webdebug = false,
) {

  if $debug {
    $sessionLogLevel = '7'
    $sessionLogCleanLevel = '0'
  } else {
    $sessionLogLevel = '6'
    $sessionLogCleanLevel = '1'
  }
  if $webdebug {
    $webSessionLogLevel = '7'
  } else {
    $webSessionLogLevel = '6'
  }
  $sessionLog = 'SessionLogLevel'
  $webSessionLog = 'WebSessionLogLevel'
  $sessionLogClean = 'SessionLogClean'
  case $node_type {
    'terminal-server-node': {
      nomachine::config_line { 'NodeSessionLog':
        type   => 'node',
        key    => $sessionLog,
        value  => $sessionLogLevel,
        after  => '#SessionLogLevel 6',
      }
      nomachine::config_line { 'ServerSessionLog':
        type   => 'server',
        key    => $sessionLog,
        value  => $sessionLogLevel,
        after  => '#SessionLogLevel 6',
      }
      nomachine::config_line { 'NodeSessionLogClean':
        type   => 'node',
        key    => $sessionLogClean,
        value  => $sessionLogCleanLevel,
        after  => '#SessionLogClean 1',
      }

    }
    'enterprise-server',
    'enterprise-terminal-server',
    'enterprise-terminal-server-cluster': {
      nomachine::config_line { 'ServerSessionLog':
        type   => 'server',
        key    => $sessionLog,
        value  => $sessionLogLevel,
        after  => '#SessionLogLevel 6',
      }
      nomachine::config_line { 'WebSessionLog':
        type   => 'server',
        key    => $webSessionLog,
        value  => $webSessionLogLevel,
        after  => '#WebSessionLogLevel 6',
      }
    }
    default: {
      fail("NX node type unknown '${node_type}'")
    }
  }

}

