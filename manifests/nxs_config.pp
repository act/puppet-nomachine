#script to configure custome web apps
define nomachine::nxs_config (
  Enum['present', 'absent'] $ensure = 'present',
  String[1] $command_line = '',
  Enum['', 'application', 'default', 'console'] $unix_desktop = '', 
  Enum['', 'xsession-default', 'kde', 'gnome', 'xdm', 'console'] $desktop = '', 
  Enum['', 'unix', 'windows', 'vnc', 'rdp'] $session = '', 

  String $value,
  String $key,   #This does not work :-(    = $title,
) {
  file  {"/usr/NX/share/config/${title}.nxs":
    ensure  => $ensure,
    content => template('nomachine/nxs_config.erb'),
    mode    => '0600',
    owner   => 'nxhtd',
    group   => 'nxhtd',
  }
}
