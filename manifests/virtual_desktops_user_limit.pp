#
class nomachine::virtual_desktops_user_limit (
  Enum['absent','present'] $ensure,
  Integer[1] $limit
) {

  nomachine::config_line { 'VirtualDesktopsUserLimit':
    ensure => $ensure,
    type   => 'server',
    key    => 'VirtualDesktopsUserLimit',
    value  => "${limit}",
    after  => '#VirtualDesktopsUserLimit 20',
  }

}

