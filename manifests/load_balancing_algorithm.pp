#
class nomachine::load_balancing_algorithm (
  Enum['absent','present'] $ensure,
  Enum['round-robin', 'custom', 'load-average'] $algorithm
) {

  nomachine::config_line { 'LoadBalancingAlgorithm':
    ensure => $ensure,
    type   => 'server',
    key    => 'LoadBalancingAlgorithm',
    value  => $algorithm,
  }

}

