#
class nomachine::disk_sharing_private_shell_profile  (
  Enum['absent','present'] $ensure = 'present',
  String $base_path,
) {

  $shell_path = regsubst($base_path,'[()]','','G')
  $content = "NXDIR=${shell_path}
DESKTOPDIR=\$HOME/Desktop
DESKTOPLINK=\$DESKTOPDIR/NX-Private-Disks

if [ -d \$DESKTOPDIR ]
then
  if [ ! -d \$NXDIR ] 
  then
    mkdir \$NXDIR
  fi

  chmod 0700 \$NXDIR

  if [ ! -L \$DESKTOPLINK ]
  then
    ln -s \$NXDIR \$DESKTOPLINK
  fi
fi
"
  file { '/etc/profile.d/nx-private-disks.sh':
    ensure => $ensure,
    content => $content,
    mode    => '0544',
    owner   => 'root',
    group   => 'root',
  }

}

