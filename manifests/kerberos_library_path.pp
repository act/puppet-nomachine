#
class nomachine::kerberos_library_path (
  Enum['absent','present'] $ensure,
  String $path = '/usr/lib/x86_64-linux-gnu/libkrb5.so.3',
) {

  nomachine::config_line { 'NXKerberosLibraryPath':
    ensure => $ensure,
    type   => 'server',
    key    => 'NXKerberosLibraryPath',
    value  => "\"${path}\"",
    after  => '#NXKerberosLibraryPath ""',
  }

}

