#
class nomachine::enable_screen_blanking (
  Enum['absent','present'] $ensure = 'present',
  String $blanking = 'true',
) {

  $value = $blanking ? {
    /(enable|true)/ => 1,
    true            => 1,
    default         => 0,
  }

  nomachine::config_line { 'EnableScreenBlanking':
    ensure => $ensure,
    type   => 'server',
    key    => 'EnableScreenBlanking',
    value  => "${value}",
    after  => '#EnableScreenBlanking 0',
  }

}

