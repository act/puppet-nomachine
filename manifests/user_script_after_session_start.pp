#
class nomachine::user_script_after_session_start (
  Enum['absent','present'] $ensure = 'present',
  String[1] $script,
) {

  nomachine::config_line { 'UserScriptAfterSessionStart':
    ensure => $ensure,
    type   => 'node',
    key    => 'UserScriptAfterSessionStart',
    value  => "\"${script}\"",
  }

}

