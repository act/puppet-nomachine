#
class nomachine::kerberos_forwarding_to_remote (
  Enum['absent','present'] $ensure,
  Integer[0,2] $mode
) {

  nomachine::config_line { 'EnableNXKerberosForwardingToRemote':
    ensure => $ensure,
    type   => 'server',
    key    => 'EnableNXKerberosForwardingToRemote',
    value  => "${mode}",
    after  => '#EnableNXKerberosForwardingToRemote 0',
  }

}

