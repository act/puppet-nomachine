# @summary create node config files 
#
# @param config
#   Hash containing system config
#
# @param ensure
#   config file is 'present' or 'absent'
#
# @param
#   user_config per user configs
#   key is the username and the value is hash containing the keys
#    - ensure 'present' or 'absent'
#    - config hashed values to override the system config
#
# @param
#   nx_base_dir path NX installation 
#
class nomachine::node (
  Enum['absent','present'] $ensure = 'present',
  Hash $config = {
    'EnableSmartcardSharing' => 1,
    'EnableCUPSSupport'      => 0,
    'CUPSBinPath'            => '""',
    'CUPSSbinPath'           => '""',
    'CUPSBackendPath'        => '""',
    'EnableSMBFSSupport'     => 1,
    'DisplayServerThreads'   => auto,
    'DisplayEncoderThreads'  => auto,
    'EnableDirectXSupport'   => 0,
  },
  Hash $user_config  = {},
  String $nx_base_dir = '/usr',
) {
  include dirtree
  #setup system node.cfg file from $config hash
  #Need to resolve conflicts with other nomachine classes and init.pp
  nomachine::node_config { 'default':
    ensure      => $user_ensure,
    config_file => "${nx_base_dir}/NX/etc/node-test.cfg",
    config      => $config,
  }
#  if $config['DiskSharingPrivateBasePath'] {
#      $privtree = dirtree($config['DiskSharingPrivateBasePath'] )
#      ensure_resource('file', $privtree, {'ensure' => 'directory'})
#  }
  #Setup per user node.cfg from $config with override from user_config
  $user_config.each |$user, $value| {
    $user_ensure =  'ensure' in $value ? {
      true => $value['ensure'],
      default => 'present',
    }
    $user_config =  'config' in $value ? {
      true => $value['config'],
      default => {},
    }
    nomachine::node_config { $user:
      ensure      => $user_ensure,
      config_file => "${nx_base_dir}/NX/etc/${user}.node.cfg",
      config      => $config + $user_config,
    }
  }
}
