#
class nomachine::session_types (
  Enum['server','node'] $node_type,
  Array $sessions =  [
    'unix-remote',
    'unix-console',
    'unix-default',
    'unix-application',
    'physical-desktop',
    'shadow',
    'unix-xsession-default',
    'unix-gnome',
    'unix-xdm',
    'windows'
    ],
) {

include stdlib

  nomachine::config_line { 'AvailableSessionTypes':
    ensure => 'present',
    type   => $node_type,
    key    => 'AvailableSessionTypes',
    value  => join($sessions,','),
  }

}

