#
class nomachine::audio_interface (
  Enum['absent','present'] $ensure = 'present',
  Enum['pulseaudio', 'alsa', 'disabled'] $interface = 'pulseaudio',
  ) {

  nomachine::config_line { 'AudioInterface':
    ensure => $ensure,
    type   => 'node',
    key    => 'AudioInterface',
    value  => $interface,
  }

}

