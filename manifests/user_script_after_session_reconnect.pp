#
class nomachine::user_script_after_session_reconnect (
  Enum['absent','present'] $ensure = 'present',
  String[1] $script,
) {

  nomachine::config_line { 'UserScriptAfterSessionReconnect':
    ensure => $ensure,
    type   => 'node',
    key    => 'UserScriptAfterSessionReconnect',
    value  => "\"${script}\"",
  }

}

