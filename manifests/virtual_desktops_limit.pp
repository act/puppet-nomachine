#
class nomachine::virtual_desktops_limit (
  Enum['absent','present'] $ensure,
  Integer[1] $limit
) {

  nomachine::config_line { 'VirtualDesktopsLimit':
    ensure => $ensure,
    type   => 'server',
    key    => 'VirtualDesktopsLimit',
    value  => "${limit}",
    after  => '#VirtualDesktopsLimit 20',
  }

}

