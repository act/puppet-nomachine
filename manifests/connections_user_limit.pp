#
class nomachine::connections_user_limit (
  Enum['absent','present'] $ensure,
  Integer[1] $limit
) {

  nomachine::config_line { 'ConnectionsUserLimit':
    ensure => $ensure,
    type   => 'server',
    key    => 'ConnectionsUserLimit',
    value  => "${limit}",
  #  after  => '#ConnectionsUserLimit 20',
  }

}

