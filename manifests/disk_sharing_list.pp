#
class nomachine::disk_sharing_list (
  Enum['absent','present'] $ensure = 'present',
  String $base_path,
) {

require profiles::nxserver
include stdlib

  nomachine::config_line { 'DiskSharingList':
    ensure => $ensure,
    type   => 'node',
    key    => 'DiskSharingList',
    value  => "\"${base_path}\"",
#    after  => '#DiskSharingList "$(DESKTOP)"',
    after  => '^#DiskSharingList ',
  }

}

