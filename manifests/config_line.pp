#
define nomachine::config_line (
  Enum[
    'enterprise-terminal-server',
    'enterprise-server',
    'terminal-server-node',
    'server',
    'node'
  ] $type,
  String $value,
  String $key,   #This does not work :-(    = $title,
  Enum['absent','present'] $ensure = 'present',
  String $after  = "#${key} "
) {

include nomachine
include stdlib

  $replace = $ensure ? {
   'absent' => false,
    default => true,
  }

  $file = $type ? {
    'enterprise-terminal-server' => 'server',
    'enterprise-server'          => 'server',
    'terminal-server-node'       => 'node',
    default                      => $type,
  }

  Class['nomachine'] -> file_line { "NX ${type} ${key}":
    ensure  => $ensure,
    path    => "/usr/NX/etc/${file}.cfg",
    line    => "${key} ${value}",
    match   => "^${key}",
    after   => $after,
    replace => $replace,
    match_for_absence => true,
  }

}

