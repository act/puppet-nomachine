#
class nomachine::connections_limit (
  Enum['absent','present'] $ensure,
  Integer[1] $limit
) {

  nomachine::config_line { 'ConnectionsLimit':
    ensure => $ensure,
    type   => 'server',
    key    => 'ConnectionsLimit',
    value  => "${limit}",
  }

}

