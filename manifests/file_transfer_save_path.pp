#
class nomachine::file_transfer_save_path (
  Enum['absent','present'] $ensure = 'present',
  String $save_path,
) {

require profiles::nxserver
include stdlib

  nomachine::config_line { 'FileTransferSavePath':
    ensure => $ensure,
    type   => 'node',
    key    => 'FileTransferSavePath',
    value  => "\"${save_path}\"",
  }

}

