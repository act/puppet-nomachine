#allow nxserver -list 
class nomachine::nxserver_list (
  Enum['absent','present'] $ensure = 'present',
  String $who,
) {

  sudo::conf { 'nxserver_list':
    content => "${who} ALL=(root) /usr/NX/bin/nexserver --list *"
  }
}
