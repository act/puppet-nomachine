#
class nomachine::display_limit (
  Enum['absent','present'] $ensure,
  Integer[1] $limit
) {

  nomachine::config_line { 'DisplayLimit':
    ensure => $ensure,
    type   => 'server',
    key    => 'DisplayLimit',
    value  => "${limit}",
    after  => '#DisplayLimit 20',
  }

}

