#Fix for Green area under chrome, matlab, etc. see #SE04007140
class nomachine::proxy_extra_options (
  Enum['absent','present'] $ensure = 'present',
  String $options,
) {

  profiles::nxserver::config_line { 'ProxyExtraOptions':
    ensure => $ensure,
    type   => 'node',
    key    => 'ProxyExtraOptions',
    value  => $options,
  }

}

