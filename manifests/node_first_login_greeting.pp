#
class nomachine::node_first_login_greeting (
  Enum['absent','present'] $ensure,
  String $greeting
) {

  nomachine::config_line { 'NodeFirstLoginGreeting':
    ensure => $ensure,
    type   => 'node',
    key    => 'NodeFirstLoginGreeting',
    value  => "\"${greeting}\"",
  }

}

