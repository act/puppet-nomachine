#allow nxserver -list 
class nomachine::shell_profile (
  Enum['absent','present'] $ensure = 'present',
) {

  file { '/etc/profile.d/nxsever.csh':
    content => '# Add /usr/NX/bin to the path for csh users
set path = ($path /usr/NX/bin)
'
  }
  file { '/etc/profile.d/nxsever.sh':
    content => '# Add /usr/NX/bin to the path for sh compatible users
if ! echo $PATH | grep -q /usr/NX/bin ; then
  export PATH=$PATH:/usr/NX/bin
fi
'
  }
}
