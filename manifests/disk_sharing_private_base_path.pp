#
class nomachine::disk_sharing_private_base_path (
  Enum['absent','present'] $ensure = 'present',
  String $base_path,
  String $mode = '1777',
) {

  include stdlib
  $dir=dirname($base_path)
  file { $dir:
    ensure => 'directory',
    mode   => $mode,
  }
  nomachine::config_line { 'DiskSharingPrivateBasePath':
    ensure => $ensure,
    type   => 'node',
    key    => 'DiskSharingPrivateBasePath',
    value  => "\"${base_path}\"",
#    after  => '#DiskSharingPrivateBasePath "$(DESKTOP)"',
    after  => '^#DiskSharingPrivateBasePath ',
  }

}

