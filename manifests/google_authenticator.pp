#setup ssh to use google_authentication
class nomachine::google_authenticator (
   Enum['present','absent'] $ensure = 'present',
   String $secret_file = '$HOME/.google_authenticator',
   String $google_pkg,
) {
 
    include ssh

    if $ensure == 'absent' {
      Package { ensure => 'absent' }
    } else {
      Package { ensure => 'installed' }
    }
    package { $google_pkg: }


    #configure sshd
#    augeas { "sshd_config":
#          context => "/files/etc/ssh/sshd_config",
#          changes => [
#                "set AuthenticationMethods keyboard-interactive",
#                "set ChallengeResponseAuthentication yes",
#              ],
#          notify => Service["sshd"],
#          require => Package['openssh-server'],
#    }

    #configure pam
    file_line { '/etc/pam.d/sshd':
          path => '/etc/pam.d/sshd',
          line => "auth       required     pam_google_authenticator.so secret=${secret_file}",
          require => Class['ssh'],
    }
}
