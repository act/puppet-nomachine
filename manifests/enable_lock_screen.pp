#
class nomachine::enable_lock_screen (
  Enum['absent','present'] $ensure = 'present',
  String $lock = 'true',
) {

  $value = $lock ? {
    /(enable|true)/ => 1,
    true            => 1,
    default         => 0,
  }

  nomachine::config_line { 'EnableLockScreen':
    ensure => $ensure,
    type   => 'server',
    key    => 'EnableLockScreen',
    value  => "${value}",
    after  => '#EnableLockScreen 0',
  }

}

