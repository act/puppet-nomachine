#
class nomachine::display_message_script (
  Enum['present', 'absent'] $ensure = 'present',
  String[1] $message,
  String[1] $script,
  String[1] $nxclient = 'nxclient',
) {
  require nomachine::custom_script_dir
  file  {"/usr/NX/scripts/custom/${script}":
    ensure  => $ensure,
    content => template('nomachine/display_message.erb'),
    mode    => '0755',
    require => Class['nomachine::custom_script_dir'],
  }
}
