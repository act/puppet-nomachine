#
class nomachine::custom_script_dir (
  String $dir = '/usr/NX/scripts/custom',
) {
  file { $dir:
    ensure => 'directory',
    mode   => '755',
    require => Class['nomachine'],
  }
}
