#
class nomachine::web_menu_tutorial (
  Enum['absent','present'] $ensure = 'present',
  String $share,
) {

  $value = $share ? {
    /(enable|true)/ => 1,
    true            => 1,
    default         => 0,
  }

  nomachine::config_line { 'EnableWebMenuTutorial':
    ensure => $ensure,
    type   => 'server',
    key    => 'EnableWebMenuTutorial',
    value  => "${value}",
#    after  => '#EnableWebMenuTutorial 1',
  }

}

