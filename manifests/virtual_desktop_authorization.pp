#
class nomachine::virtual_desktop_authorization (
  Enum['absent','present'] $ensure,
  Integer[0,2] $mode
) {

  nomachine::config_line { 'VirtualDesktopAuthorization':
    ensure => $ensure,
    type   => 'server',
    key    => 'VirtualDesktopAuthorization',
    value  => "${mode}",
    after  => '#VirtualDesktopAuthorization 1',
  }

}

