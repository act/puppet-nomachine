#
class nomachine::enable_network_broadcast (
  Enum['absent','present'] $ensure = 'present',
  String $broadcast = 'true',
) {

  $value = $broadcast ? {
    /(enable|true)/ => 1,
    true            => 1,
    default         => 0,
  }

  nomachine::config_line { 'EnableNetworkBroadcast':
    ensure => $ensure,
    type   => 'server',
    key    => 'EnableNetworkBroadcast',
    value  => "${value}",
    after  => '#EnableNetworkBroadcast 1',
  }

}

