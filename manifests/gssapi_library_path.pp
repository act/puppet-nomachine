#
class nomachine::gssapi_library_path (
  Enum['absent','present'] $ensure,
  String $path = '/usr/lib/x86_64-linux-gnu/libgssapi_krb5.so.2',
) {

  nomachine::config_line { 'NXGssapiLibraryPath':
    ensure => $ensure,
    type   => 'server',
    key    => 'NXGssapiLibraryPath',
    value  => "\"${path}\"",
    after  => '#NXGssapiLibraryPath ""',
  }

}

