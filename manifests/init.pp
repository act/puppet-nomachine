#
class nomachine (
  Enum['absent','present','latest'] $ensure,
  Enum[
    'terminal-server-node',
    'enterprise-terminal-server-node',
    'free',
    'cloud-server',
    'enterprise-desktop',
    'enterprise-server',
    'enterprise-terminal-server',
    'enterprise-terminal-server-cluster',
    'terminal-server'
    ] $node_type,
  String $release,
  String $pkg_location, 
  Enum['gnome','ubuntu','unity','xubuntu','xfce','mate','cloudtop'] 
    $desktop_distribution = 'gnome',
) {

  case $::osfamily {
    'Debian': {
      $provider = 'dpkg'
      $filetype = 'deb'
    }
    'RedHat': {
      $provider = 'rpm'
      $filetype = 'rpm'
    }
    default: {
      fail("NX does not support ${::osfamily}")
    }
  }

  case $node_type {
    'terminal-server-node',
    'enterprise-terminal-server-node',
    'free': {
      if $node_type == 'free' {
        $pkgname  = 'nomachine'
        $install_file = "nomachine_${release}_${::architecture}.${filetype}"
      } else {
        $pkgname  = "nomachine-${node_type}"
        $install_file = "nomachine-${node_type}_${release}_${::architecture}.${filetype}"
      }
    }
    'cloud-server',
    'terminal-server',
    'enterprise-desktop',
    'enterprise-server',
    'enterprise-terminal-server',
    'enterprise-terminal-server-cluster': {
      $pkgname  = "nomachine-${node_type}"
      $install_file = "nomachine-${node_type}_${release}_${::architecture}.${filetype}"
    }
    default: {
      fail("NX node type unknown '${node_type}'")
    }
  }

  #Required to build USB module
  if $filetype == 'deb' {
    $prereq_pkgs = [ "linux-headers-${::kernelrelease}", 'gcc', 'make' ]
  } else {
    $prereq_pkgs = [ 'kernel-devel', 'gcc', 'make' ]
  }


  #Make gnome classic default mode
  case $desktop_distribution {
    ubuntu,unity:   {
      $gnomecmd='"/etc/X11/Xsession \'gnome-session --session=ubuntu\'"'
    }
    gnome:          {
      $gnomecmd='"/usr/bin/env GNOME_SHELL_SESSION_MODE=classic /usr/bin/gnome-session --session=gnome-classic"'
    }
    xubuntu,xfce:   {
      $gnomecmd='"/usr/bin/startxfce4"'
    }
    mate,cloudtop:       {
      $gnomecmd='"/etc/X11/Xsession mate-session"'
    }
    default:        {
      fail("NX server not supported on distribution ${desktop_distribution}.")
    }
  }

  unless $ensure == 'absent' {
    file_line { 'DefaultDesktopCommand':
      path    => '/usr/NX/etc/node.cfg',
      line    => "DefaultDesktopCommand ${gnomecmd}",
      match   => '^DefaultDesktopCommand',
      require => Package[$pkgname],
    }
    file_line { 'CommandStartGnome':
      path    => '/usr/NX/etc/node.cfg',
      line    => "CommandStartGnome ${gnomecmd}",
      match   => '^CommandStartGnome',
      require => Package[$pkgname],
    }
  }

#  package { $prereq_pkgs: ensure => 'installed' }
#    ->
   ensure_packages ( $prereq_pkgs,
    {
      ensure => 'installed',
      before => Package[$pkgname],
    }
  )
  package {$pkgname:
    ensure   => $ensure,
    provider => $provider,
    source   => "${pkg_location}/${install_file}",
  }

}

