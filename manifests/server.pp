# @summary create node config files 
#
# @param config Hash containing system config
#
# @param ensure config file is 'present' or 'absent'
#
# @param nx_base_dir path NX installation 
#
# @param config_template
#    puppet erb tempate to use to generate config file from hash

class nomachine::server (
  Enum['absent','present'] $ensure = 'present',
  Hash $config = {
    'EnablePasswordDB' => 0,
    'Server' => {
       'Name'           =>  '"Connection to localhost"',
       'Host'           =>  '127.0.0.1',
       'Protocol'       =>  'NX',
       'Port'           =>  4000,
       'Authentication' =>  'password',
    }
  },
  Hash $user_config  = {},
  String $nx_base_dir = '/usr',
  String $config_template = 'nomachine/server.erb',
) {

  #setup system server.cfg file from $config hash
  file { "${nx_base_dir}/NX/etc/server.cfg":
    ensure  => $ensure,
    owner   => 'nx',
    group   => 'nx',
    mode    => '0644',
    content => template($config_template),
    require => Class['nomachine'],
  }

}
