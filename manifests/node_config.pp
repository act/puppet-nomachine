# @summary create node for for a user
#
# @param config_file path name of config
#
# @param config Hash containg config
#
# @param ensure config file is 'present' or 'absent'

define nomachine::node_config (
  Enum['absent','present'] $ensure = 'present',
  String $config_file = '/usr/NX/etc/node.cfg',
  Hash $config = {
    'EnableSmartcardSharing' => 1,
    'EnableCUPSSupport'      => 0,
    'CUPSBinPath'            => '""',
    'CUPSSbinPath'           => '""',
    'CUPSBackendPath'        => '""',
    'EnableSMBFSSupport'     => 1,
    'DisplayServerThreads'   => auto,
    'DisplayEncoderThreads'  => auto,
    'EnableDirectXSupport'   => 0,
  },
  String $config_template = 'nomachine/node.erb',
) {

include nomachine
include stdlib

  file { $config_file:
    ensure  => $ensure,
    path    => $config_file,
    owner   => 'nx',
    group   => 'nx',
    mode    => '0644',
    content => template($config_template),
    require => Class['nomachine'],
  }

}

