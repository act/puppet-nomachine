#
class nomachine::agent_x11_vector_graphics (
  Enum['absent','present'] $ensure = 'present',
  Boolean $enabled = true,
) {

include stdlib

  $value = $enabled ? {
    false   => '0',
    default => '1',
  }
  nomachine::config_line { 'AgentX11VectorGraphics':
    ensure => $ensure,
    type   => 'node',
    key    => 'AgentX11VectorGraphics',
    value  => $value,
  }

}

